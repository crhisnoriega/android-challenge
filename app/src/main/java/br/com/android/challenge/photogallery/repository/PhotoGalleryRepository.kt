package br.com.android.challenge.photogallery.repository

import br.com.android.challenge.photogallery.api.ImgUrAPI
import br.com.android.challenge.photogallery.model.PhotoData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


interface PhotoGalleryRepository {
    fun fetchPhotos(): Flow<List<PhotoData>>
}

class PhotoGalleryRepositoryImpl(private val eventsAPI: ImgUrAPI) :
    PhotoGalleryRepository {

    override fun fetchPhotos() = flow {
        val result = eventsAPI.fetchEvents().data
        emit(result)
    }
}