package br.com.android.challenge.photogallery.ui

import android.os.Bundle
import br.com.android.challenge.R
import br.com.android.challenge.base.BaseActivity
import br.com.android.challenge.photogallery.di.PhotoGalleryModule
import br.com.android.challenge.photogallery.ui.fragments.PhotoGalleryFragment
import org.koin.core.module.Module

class PhotoGalleryActivity : BaseActivity() {

    override val modules: List<Module> = listOf(PhotoGalleryModule.modules)
    override val contentView = R.layout.main_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showEventsList()
    }

    override fun onBackPressed() {
        showEventsList()
    }

    private fun showEventsList() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, PhotoGalleryFragment.newInstance())
            .commit()
    }
}