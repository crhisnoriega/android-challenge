package br.com.android.challenge.photogallery.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PhotoSearchResponse(
    @SerializedName("data") val data: List<PhotoData>
) : Parcelable