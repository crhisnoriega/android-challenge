package br.com.android.challenge.photogallery.business

import br.com.android.challenge.base.FlowUseCase
import br.com.android.challenge.data.Resources
import br.com.android.challenge.data.ViewState
import br.com.android.challenge.data.safeAPICallLiveData
import br.com.android.challenge.photogallery.model.ImageData
import br.com.android.challenge.photogallery.repository.PhotoGalleryRepository
import kotlinx.coroutines.flow.map

class FetchPhotosUseCase(
    private val photoGalleryRepository: PhotoGalleryRepository
) : FlowUseCase<Unit, ViewState>() {
    override suspend fun performAction(params: Unit?) =
        safeAPICallLiveData { photoGalleryRepository.fetchPhotos() }.map {
            when (it) {
                is Resources.Loading -> ViewState.Loading
                is Resources.Success<*> -> ViewState.Success(it.data)
                is Resources.Error -> ViewState.Error(it.error)
            }
        }


}