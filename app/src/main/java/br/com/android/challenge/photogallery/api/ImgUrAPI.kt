package br.com.android.challenge.photogallery.api

import br.com.android.challenge.photogallery.model.PhotoSearchResponse
import retrofit2.http.GET

interface ImgUrAPI {
    @GET("gallery/search/?q=cats")
    suspend fun fetchEvents(
    ): PhotoSearchResponse
}