package br.com.android.challenge.photogallery.persistence

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow


@Dao
interface PhotoDAO {

    @Query("SELECT * FROM PhotoEntity")
    fun getAll(): Flow<List<PhotoEntity>>

}
