package br.com.android.challenge.photogallery.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import br.com.android.challenge.databinding.ItemPhotoLayoutBinding
import br.com.android.challenge.photogallery.model.PhotoData
import com.bumptech.glide.Glide

class ItemPhotoViewHolder(private val itemEventLayoutBinding: ItemPhotoLayoutBinding) :
    RecyclerView.ViewHolder(itemEventLayoutBinding.root) {

    fun bind(
        photoData: PhotoData,
        onClick: ((photoData: PhotoData) -> Unit)?
    ) {
        Glide
            .with(itemEventLayoutBinding.root.context)
            .load(photoData.images?.getOrNull(0)?.link)
            .centerCrop()
            .into(itemEventLayoutBinding.image)
        itemEventLayoutBinding.image.setOnClickListener {
            onClick?.invoke(photoData)
        }
    }

}