package br.com.android.challenge.photogallery.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PhotoData(
    @SerializedName("id") val id: String? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("description") val description: String? = null,
    @SerializedName("link") val link: String? = null,
    @SerializedName("images") val images: List<ImageData>? = null
) : Parcelable