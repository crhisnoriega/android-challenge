package br.com.android.challenge.photogallery.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.android.challenge.data.ViewState
import br.com.android.challenge.photogallery.business.FetchPhotosUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class PhotoGalleryViewModel(
    private val fetchEventsUseCase: FetchPhotosUseCase
) :
    ViewModel() {

    private val _listResult = MutableStateFlow<ViewState>(ViewState.Empty)

    var listResult: StateFlow<ViewState> = _listResult

    fun fetchPhotos() {
        viewModelScope.launch {
            fetchEventsUseCase.resultFlow.collect {
                _listResult.emit(it)
            }

            fetchEventsUseCase.launch(Unit)
        }
    }

}