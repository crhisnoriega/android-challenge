package br.com.android.challenge.photogallery.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.android.challenge.base.BaseFragment
import br.com.android.challenge.data.ViewState
import br.com.android.challenge.databinding.FragmentPhotoGalleryBinding
import br.com.android.challenge.photogallery.model.ImageData
import br.com.android.challenge.photogallery.model.PhotoData
import br.com.android.challenge.photogallery.ui.adapter.PhotoListAdapter
import br.com.android.challenge.photogallery.viewmodel.PhotoGalleryViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class PhotoGalleryFragment : BaseFragment() {

    private val viewModel by viewModel<PhotoGalleryViewModel>()

    private lateinit var binding: FragmentPhotoGalleryBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPhotoGalleryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        configureObservers()
        configurePhotoList()

        viewModel.fetchPhotos()
    }

    private fun configureObservers() {
        lifecycleScope.launch {
            viewModel.listResult.collect { data ->
                when (data) {
                    is ViewState.Loading -> binding.progress.visibility = View.VISIBLE
                    is ViewState.Success<*> -> {
                        populateList(data.data as List<PhotoData>)
                        binding.progress.visibility = View.GONE
                    }
                    is ViewState.Error -> {
                        binding.progress.visibility = View.GONE
                        showError(data.error.message, binding.snackBar)
                    }
                    else -> {
                    }
                }
            }
        }
    }

    private fun configurePhotoList() {
        binding.rvPhotos.apply {
            layoutManager = GridLayoutManager(requireContext(), 4)
            adapter =
                PhotoListAdapter(
                    mutableListOf()
                ) {}
        }
    }

    private fun populateList(photos: List<PhotoData>) {
        (binding.rvPhotos.adapter as PhotoListAdapter).apply {
            photoList.addAll(photos.toMutableList())
            notifyDataSetChanged()
        }
    }


    companion object {
        fun newInstance() =
            PhotoGalleryFragment()
    }

}