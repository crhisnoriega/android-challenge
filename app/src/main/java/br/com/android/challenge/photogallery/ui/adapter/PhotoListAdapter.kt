package br.com.android.challenge.photogallery.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.android.challenge.databinding.ItemPhotoLayoutBinding
import br.com.android.challenge.photogallery.model.PhotoData
import br.com.android.challenge.photogallery.ui.viewholder.ItemPhotoViewHolder

class PhotoListAdapter(
    val photoList: MutableList<PhotoData>,
    private val onClick: ((currencyEntity: PhotoData) -> Unit)?
) :
    RecyclerView.Adapter<ItemPhotoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemPhotoViewHolder(
            ItemPhotoLayoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun getItemCount() = photoList.size

    override fun onBindViewHolder(holder: ItemPhotoViewHolder, position: Int) {
        holder.bind(photoList[position]) {

        }
    }
}