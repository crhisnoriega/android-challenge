package br.com.android.challenge.data

import androidx.room.Database
import androidx.room.RoomDatabase
import br.com.android.challenge.photogallery.persistence.PhotoDAO
import br.com.android.challenge.photogallery.persistence.PhotoEntity

const val DATABASE_NAME = "events-db"
@Database
    (entities = [PhotoEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun eventDAO(): PhotoDAO
}