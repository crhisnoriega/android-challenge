package br.com.android.challenge.photogallery.di

import androidx.room.Room
import br.com.android.challenge.data.AppDatabase
import br.com.android.challenge.data.DATABASE_NAME
import br.com.android.challenge.photogallery.api.ImgUrAPI
import br.com.android.challenge.photogallery.business.FetchPhotosUseCase
import br.com.android.challenge.photogallery.repository.PhotoGalleryRepository
import br.com.android.challenge.photogallery.repository.PhotoGalleryRepositoryImpl
import br.com.android.challenge.photogallery.viewmodel.PhotoGalleryViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object PhotoGalleryModule {
    val modules = module {
        // api
        factory { providerImgUrlAPI(get()) }

        // repository
        factory<PhotoGalleryRepository> { PhotoGalleryRepositoryImpl(get()) }

        // business
        factory { FetchPhotosUseCase(get()) }

        // database - DAO
        single {
            Room.databaseBuilder(
                androidContext(),
                AppDatabase::class.java,
                DATABASE_NAME
            ).build()
        }
        single { get<AppDatabase>().eventDAO() }

        // view models
        viewModel { PhotoGalleryViewModel(get()) }
    }

    private fun providerImgUrlAPI(retrofit: Retrofit) = retrofit.create(ImgUrAPI::class.java)
}